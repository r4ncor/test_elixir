defmodule BooksApi.DirectoryTest do
  use BooksApi.DataCase

  alias BooksApi.Directory

  describe "authors" do
    alias BooksApi.Directory.Author

    @valid_attrs %{firstname: "some firstname", lastname: "some lastname"}
    @update_attrs %{firstname: "some updated firstname", lastname: "some updated lastname"}
    @invalid_attrs %{firstname: nil, lastname: nil}

    def author_fixture(attrs \\ %{}) do
      {:ok, author} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Directory.create_author()

      author
    end

    test "list_authors/0 returns all authors" do
      author = author_fixture()
      assert Directory.list_authors() == [author]
    end

    test "get_author!/1 returns the author with given id" do
      author = author_fixture()
      assert Directory.get_author!(author.id) == author
    end

    test "create_author/1 with valid data creates a author" do
      assert {:ok, %Author{} = author} = Directory.create_author(@valid_attrs)
      assert author.firstname == "some firstname"
      assert author.lastname == "some lastname"
    end

    test "create_author/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Directory.create_author(@invalid_attrs)
    end

    test "update_author/2 with valid data updates the author" do
      author = author_fixture()
      assert {:ok, author} = Directory.update_author(author, @update_attrs)
      assert %Author{} = author
      assert author.firstname == "some updated firstname"
      assert author.lastname == "some updated lastname"
    end

    test "update_author/2 with invalid data returns error changeset" do
      author = author_fixture()
      assert {:error, %Ecto.Changeset{}} = Directory.update_author(author, @invalid_attrs)
      assert author == Directory.get_author!(author.id)
    end

    test "delete_author/1 deletes the author" do
      author = author_fixture()
      assert {:ok, %Author{}} = Directory.delete_author(author)
      assert_raise Ecto.NoResultsError, fn -> Directory.get_author!(author.id) end
    end

    test "change_author/1 returns a author changeset" do
      author = author_fixture()
      assert %Ecto.Changeset{} = Directory.change_author(author)
    end
  end

  describe "books" do
    alias BooksApi.Directory.Book

    @valid_attrs %{description: "some description", name: "some name"}
    @update_attrs %{description: "some updated description", name: "some updated name"}
    @invalid_attrs %{description: nil, name: nil}

    def book_fixture(attrs \\ %{}) do
      {:ok, book} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Directory.create_book()

      book
    end

    test "list_books/0 returns all books" do
      book = book_fixture()
      assert Directory.list_books() == [book]
    end

    test "get_book!/1 returns the book with given id" do
      book = book_fixture()
      assert Directory.get_book!(book.id) == book
    end

    test "create_book/1 with valid data creates a book" do
      assert {:ok, %Book{} = book} = Directory.create_book(@valid_attrs)
      assert book.description == "some description"
      assert book.name == "some name"
    end

    test "create_book/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Directory.create_book(@invalid_attrs)
    end

    test "update_book/2 with valid data updates the book" do
      book = book_fixture()
      assert {:ok, book} = Directory.update_book(book, @update_attrs)
      assert %Book{} = book
      assert book.description == "some updated description"
      assert book.name == "some updated name"
    end

    test "update_book/2 with invalid data returns error changeset" do
      book = book_fixture()
      assert {:error, %Ecto.Changeset{}} = Directory.update_book(book, @invalid_attrs)
      assert book == Directory.get_book!(book.id)
    end

    test "delete_book/1 deletes the book" do
      book = book_fixture()
      assert {:ok, %Book{}} = Directory.delete_book(book)
      assert_raise Ecto.NoResultsError, fn -> Directory.get_book!(book.id) end
    end

    test "change_book/1 returns a book changeset" do
      book = book_fixture()
      assert %Ecto.Changeset{} = Directory.change_book(book)
    end
  end

  describe "book_authors" do
    alias BooksApi.Directory.BookAuthor

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def book_author_fixture(attrs \\ %{}) do
      {:ok, book_author} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Directory.create_book_author()

      book_author
    end

    test "list_book_authors/0 returns all book_authors" do
      book_author = book_author_fixture()
      assert Directory.list_book_authors() == [book_author]
    end

    test "get_book_author!/1 returns the book_author with given id" do
      book_author = book_author_fixture()
      assert Directory.get_book_author!(book_author.id) == book_author
    end

    test "create_book_author/1 with valid data creates a book_author" do
      assert {:ok, %BookAuthor{} = book_author} = Directory.create_book_author(@valid_attrs)
    end

    test "create_book_author/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Directory.create_book_author(@invalid_attrs)
    end

    test "update_book_author/2 with valid data updates the book_author" do
      book_author = book_author_fixture()
      assert {:ok, book_author} = Directory.update_book_author(book_author, @update_attrs)
      assert %BookAuthor{} = book_author
    end

    test "update_book_author/2 with invalid data returns error changeset" do
      book_author = book_author_fixture()
      assert {:error, %Ecto.Changeset{}} = Directory.update_book_author(book_author, @invalid_attrs)
      assert book_author == Directory.get_book_author!(book_author.id)
    end

    test "delete_book_author/1 deletes the book_author" do
      book_author = book_author_fixture()
      assert {:ok, %BookAuthor{}} = Directory.delete_book_author(book_author)
      assert_raise Ecto.NoResultsError, fn -> Directory.get_book_author!(book_author.id) end
    end

    test "change_book_author/1 returns a book_author changeset" do
      book_author = book_author_fixture()
      assert %Ecto.Changeset{} = Directory.change_book_author(book_author)
    end
  end
end
