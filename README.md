# BooksApi


git clone 
mix deps.get
mix ecto.create && mix ecto.migrate

Populate database
mix run priv/repo/seeds.exs

Run server
mix phoenix.server

Get request
http://localhost:4000/api/books/1
http://localhost:4000/api/books
http://localhost:4000/api/authors
http://localhost:4000/api/authors/1

Registro
curl -X POST http://localhost:4000/api/users/signup -H "Content-Type: application/json" -d '{"user": {"username": "user1@a.com", "password": "password"}}'
{"token":"eyJhbGciOiJIUzUxMi...", "email":"user1@a.com"}

Login
curl -X POST http://localhost:4000/api/users/signin -H "Content-Type: application/json" -d '{"username": "user1@a.com", "password": "password"}'
{"token":"eyJhbGciOiJIUzUxMi...", "email":"user1@a.com"}

Create Author
POST /api/books HTTP/1.1
Host: localhost:4000
Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJib29rc19hcGkiLCJleHAiOjE1NjczNTU2MDQsImlhdCI6MTU2NDkzNjQwNCwiaXNzIjoiYm9va3NfYXBpIiwianRpIjoiOTdhMmYyODctZjM5MC00MGI1LWI4Y2UtZGVhZTU1OGNlMzA0IiwibmJmIjoxNTY0OTM2NDAzLCJzdWIiOiIxIiwidHlwIjoiYWNjZXNzIn0.H4lPphhmC57yLqiflsl63hF3cOZfjEaEZBlCYyVz8UKoc1oSrdP35YaOJ7eSkyJKtFKyd92sBFmf2Jia53N3Iw
Content-Type: application/json
User-Agent: PostmanRuntime/7.11.0
Accept: */*
Cache-Control: no-cache
Postman-Token: 36524f3e-528a-4709-9948-8e713d5f4b9f,13e376fe-6d21-40db-a5d7-42988286b4b4
Host: localhost:4000
accept-encoding: gzip, deflate
content-length: 88
Connection: keep-alive
cache-control: no-cache

{"book": {"name": "Yet another company 5", "description": "Another short description!"}}