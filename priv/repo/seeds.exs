# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     BooksApi.Repo.insert!(%BooksApi.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias BooksApi.Repo
alias BooksApi.Directory.Author
alias BooksApi.Directory.Book
alias BooksApi.Directory.BookAuthor
Repo.insert! %Author{firstname: "Marco", lastname: "Pascale"}
Repo.insert! %Author{firstname: "Magly", lastname: "Mendez"}
Repo.insert! %Author{firstname: "Sofia", lastname: "Pascale"}
Repo.insert! %Book{name: "Book1", description: "Short description ..."}
Repo.insert! %Book{name: "Book2", description: "Short description ..."}
Repo.insert! %Book{name: "Book3", description: "Short description ..."}
Repo.insert! %BookAuthor{book_id: 1, author_id: 1}
Repo.insert! %BookAuthor{book_id: 2, author_id: 1}
Repo.insert! %BookAuthor{book_id: 3, author_id: 2}