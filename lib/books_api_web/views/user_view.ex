defmodule BooksApiWeb.UserView do
  use BooksApiWeb, :view
  alias BooksApiWeb.UserView

  def render("user.json", %{user: user,token: token}) do
    %{id: user.id,
      username: user.username,
      token: token}
  end
end
