defmodule BooksApiWeb.Router do
  use BooksApiWeb, :router

  pipeline :auth do
    plug BooksApiWeb.Auth.Pipeline
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", BooksApiWeb do
    pipe_through :api
    post("/users/signin", UserController, :signin)
    post("/users/signup", UserController, :create)
    resources "/authors", AuthorController, only: [:index, :show]
    resources "/books", BookController, only: [:index, :show]
  end

  scope "/api", BooksApiWeb do
    pipe_through [:api, :auth]
    resources "/authors", AuthorController, except: [:index, :show]
    resources "/books", BookController, except: [:index, :show]
  end

  pipeline :browser do
    plug(:accepts, ["html"])
  end

  scope "/", BooksApiWeb do
    pipe_through :browser
    get "/", DefaultController, :index
  end
end
