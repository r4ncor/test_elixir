defmodule BooksApiWeb.DefaultController do
  
  use BooksApiWeb, :controller

  def index(conn, _params) do
    text conn, "BooksApiWeb!"
  end
end