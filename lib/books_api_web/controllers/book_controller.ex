defmodule BooksApiWeb.BookController do
  use BooksApiWeb, :controller

  alias BooksApi.Directory
  alias BooksApi.Directory.Book

  action_fallback BooksApiWeb.FallbackController

  def index(conn, _params) do
    books = Directory.list_books()
    render(conn, "index.json", books: books)
  end

  def create(conn, %{"book" => book_params}) do
    with {:ok, %Book{} = book} <- Directory.create_book(book_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", book_path(conn, :show, book))
      |> render("show.json", book: Directory.get_book!(book.id))
    end
  end

  def show(conn, %{"id" => id}) do
    book = Directory.get_book!(id)
    render(conn, "show.json", book: book)
  end

  def update(conn, %{"id" => id, "book" => book_params}) do
    book = Directory.get_book!(id)

    with {:ok, %Book{} = book} <- Directory.update_book(book, book_params) do
      render(conn, "show.json", book: book)
    end
  end

  def delete(conn, %{"id" => id}) do
    book = Directory.get_book!(id)
    with {:ok, %Book{}} <- Directory.delete_book(book) do
      send_resp(conn, :no_content, "")
    end
  end
end
