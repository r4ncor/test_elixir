defmodule BooksApi.Directory do
  @moduledoc """
  The Directory context.
  """

  import Ecto.Query, warn: false
  alias BooksApi.Repo

  alias BooksApi.Directory.Author
  alias BooksApi.Directory.Book

  @doc """
  Returns the list of authors.

  ## Examples

      iex> list_authors()
      [%Author{}, ...]

  """
  def list_authors do
    Repo.all(Author) |> Repo.preload(:books)
  end

  @doc """
  Gets a single author.

  Raises `Ecto.NoResultsError` if the Author does not exist.

  ## Examples

      iex> get_author!(123)
      %Author{}

      iex> get_author!(456)
      ** (Ecto.NoResultsError)

  """
  def get_author!(id), do: Repo.get!(Author, id) |> Repo.preload(:books)

  @doc """
  Creates a author.

  ## Examples

      iex> create_author(%{field: value})
      {:ok, %Author{}}

      iex> create_author(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_author(attrs \\ %{}) do
    %Author{}
    |> Author.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a author.

  ## Examples

      iex> update_author(author, %{field: new_value})
      {:ok, %Author{}}

      iex> update_author(author, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_author(%Author{} = author, attrs) do
    author
    |> Author.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Author.

  ## Examples

      iex> delete_author(author)
      {:ok, %Author{}}

      iex> delete_author(author)
      {:error, %Ecto.Changeset{}}

  """
  def delete_author(%Author{} = author) do
    Repo.delete(author)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking author changes.

  ## Examples

      iex> change_author(author)
      %Ecto.Changeset{source: %Author{}}

  """
  def change_author(%Author{} = author) do
    Author.changeset(author, %{})
  end

  alias BooksApi.Directory.Book

  @doc """
  Returns the list of books.

  ## Examples

      iex> list_books()
      [%Book{}, ...]

  """
  def list_books do
    Repo.all(Book) |> Repo.preload(:authors)
  end

  @doc """
  Gets a single book.

  Raises `Ecto.NoResultsError` if the Book does not exist.

  ## Examples

      iex> get_book!(123)
      %Book{}

      iex> get_book!(456)
      ** (Ecto.NoResultsError)

  """
  def get_book!(id), do: Repo.get!(Book, id) |> Repo.preload(:authors)

  @doc """
  Creates a book.

  ## Examples

      iex> create_book(%{field: value})
      {:ok, %Book{}}

      iex> create_book(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_book(attrs \\ %{}) do
    %Book{}
    |> Book.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a book.

  ## Examples

      iex> update_book(book, %{field: new_value})
      {:ok, %Book{}}

      iex> update_book(book, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_book(%Book{} = book, attrs) do
    book
    |> Book.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Book.

  ## Examples

      iex> delete_book(book)
      {:ok, %Book{}}

      iex> delete_book(book)
      {:error, %Ecto.Changeset{}}

  """
  def delete_book(%Book{} = book) do
    Repo.delete(book)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking book changes.

  ## Examples

      iex> change_book(book)
      %Ecto.Changeset{source: %Book{}}

  """
  def change_book(%Book{} = book) do
    Book.changeset(book, %{})
  end

  alias BooksApi.Directory.BookAuthor

  @doc """
  Returns the list of book_authors.

  ## Examples

      iex> list_book_authors()
      [%BookAuthor{}, ...]

  """
  def list_book_authors do
    Repo.all(BookAuthor)
  end

  @doc """
  Gets a single book_author.

  Raises `Ecto.NoResultsError` if the Book author does not exist.

  ## Examples

      iex> get_book_author!(123)
      %BookAuthor{}

      iex> get_book_author!(456)
      ** (Ecto.NoResultsError)

  """
  def get_book_author!(id), do: Repo.get!(BookAuthor, id)

  @doc """
  Creates a book_author.

  ## Examples

      iex> create_book_author(%{field: value})
      {:ok, %BookAuthor{}}

      iex> create_book_author(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_book_author(attrs \\ %{}) do
    %BookAuthor{}
    |> BookAuthor.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a book_author.

  ## Examples

      iex> update_book_author(book_author, %{field: new_value})
      {:ok, %BookAuthor{}}

      iex> update_book_author(book_author, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_book_author(%BookAuthor{} = book_author, attrs) do
    book_author
    |> BookAuthor.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a BookAuthor.

  ## Examples

      iex> delete_book_author(book_author)
      {:ok, %BookAuthor{}}

      iex> delete_book_author(book_author)
      {:error, %Ecto.Changeset{}}

  """
  def delete_book_author(%BookAuthor{} = book_author) do
    Repo.delete(book_author)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking book_author changes.

  ## Examples

      iex> change_book_author(book_author)
      %Ecto.Changeset{source: %BookAuthor{}}

  """
  def change_book_author(%BookAuthor{} = book_author) do
    BookAuthor.changeset(book_author, %{})
  end
end
