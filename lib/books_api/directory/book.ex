defmodule BooksApi.Directory.Book do
  use Ecto.Schema
  import Ecto.Changeset
  alias BooksApi.Directory.{BookAuthor, Author}

  @derive {Poison.Encoder, only: [:name, :description]}
  schema "books" do
    field :description, :string
    field :name, :string
    many_to_many(
      :authors,
      Author,
      join_through: "book_authors",
      on_replace: :delete
    )
    timestamps()
  end

  @doc false
  def changeset(book, attrs) do
    book
    |> cast(attrs, [:name, :description])
    |> validate_required([:name, :description])
  end
end
