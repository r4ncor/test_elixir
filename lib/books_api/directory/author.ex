defmodule BooksApi.Directory.Author do
  use Ecto.Schema
  import Ecto.Changeset
  alias BooksApi.Directory.{BookAuthor, Book}
  @derive {Poison.Encoder, only: [:firstname, :lastname]}
  
  schema "authors" do
    field :firstname, :string
    field :lastname, :string
    many_to_many(
      :books,
      Book,
      join_through: "book_authors",
      on_replace: :delete
    )
    timestamps()
  end

  @doc false
  def changeset(author, attrs) do
    author
    |> cast(attrs, [:firstname, :lastname])
    |> validate_required([:firstname, :lastname])
  end
end
