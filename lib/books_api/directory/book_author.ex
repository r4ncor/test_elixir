defmodule BooksApi.Directory.BookAuthor do
  use Ecto.Schema
  import Ecto.Changeset
  alias BooksApi.Directory.{BookAuthor, Book, Author}

  schema "book_authors" do
    belongs_to(:book, Book)
    belongs_to(:author, Author)

    timestamps()
  end

  @doc false
  def changeset(book_author, attrs) do
    book_author
    |> cast(attrs, [])
    |> validate_required([])
  end
end
